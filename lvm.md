**Configure local storage**
List, create, delete partitions on MBR and GPT disks
 *Create and remove physical volumes, assign physical volumes to volume groups, and create and delete logical volumes*
 *Configure systems to mount file systems at boot by Universally Unique ID (UUID) or label*
 *Add new partitions and logical volumes, and swap to a system non-destructively*

**Create and configure file systems**
Create, mount, unmount, and use vfat, ext4, and xfs file systems
Mount and unmount CIFS and NFS network file systems
 *Extend existing logical volumes*
Create and configure set-GID directories for collaboration
 *Create and manage Access Control Lists (ACLs)*
Diagnose and correct file permission problems

---------------------------------------------------------------------
##  Create and remove physical volumes
		-Assumes local disk or SAN disk presented with multipath working
		-echo "- - -" > /sys/class/scsi_host/host0/scan (scan bus)
		-PV is a logical construct made up of one or more real disks (dsf)
		-What to do to scan the bus- new script in 7 that comes with dmmultipath
		-New GUI utility to configure LVs
		-To scan for block devices to use as a PV
			-lvmdiskscan
			-fdisk -l
		-To view PVs 
		   -pvscan
		   -pvdisplay
		   -pvs
		   -fdisk -l (used to not see LVM info like you do now)
		-pvcreate /dev/sdb1 (IDE=hd SATA=sd VirtDisk=vd, letter is which drive, number is which partiton on that drive)
		-PV gives UUID to block device for LVM
		-you can create mutiple PVs on same line
  ## CREATE PV
  		* pvcreate </dev/dsf>

  ## REMOVE PV
  		* pvremove </dev/dsf>

---
    

##  Assign physical volumes to volume groups
		-vgcreate <name of VG> /dev/vdx (put multiple disks of the same line if you want to came the VG up of more than one disk)
		-vgchange to alter VG properties.
		-vgextend to add a disk to a VG
		-lvcreate -L?G -n <?LV> <existingVG> (or can be by LE or %)(don't care about stripe size)
		-To view VGs/LVs
			-vgscan/lvscan
			-vgdisplay
			-lvdisplay
			-dmsetup info -c

  ## ASSIGN PV TO VG
  		* vgcreate <pv>

---
    

##  Create and delete logical volumes 
		-multiple commands to create FS is various types. write it to the mapped file once it's a lvol
		-which mkfs.ext4; ls /sbin| grep mk
		-mkfs with -L to add label
		-removing VG will remove LV and FS
		-lvremove with just name of vg will remove all lvs in that vg

  ## CREATE LV
  		* lvcreate -L <size> -n <LVname> <vg>

  ## DELETE LV
  		* lvremove <vg/lv>

---
    

##  Additional
		-New GUI tool for LVM and how to install
		-static devices vs mapped devices- we only care about static
		-man -k lv & man -k vg
---
    

##  Configure systems to mount file systems at boot by Universally Unique ID (UUID) or label 
		-'blkid' to determine UUID of device (the UUIDs in lvs seem not right)
  ## FIND UUID
  		* blkid, ls -lR /dev/disk
  ## ADD LINE TO FSTAB
  		* UUID=<uuid> /mount_point ext4 defaults 1 2
  ## MOUNT
  		* mount -a
  		* mounts

---
    

##  Add new partitions and logical volumes, and swap to a system non-destructively
		-use fdisk (parted?) to create partitions on disks, with labels
		-use fdisk to create swap partition and mkswap to swap it on (can you swapon a whole disk without a partition?) 
  ## ABOVE PROCS ADD PARTS AND LVs NON-DESTRUCTIVLY
  ## ADD SWAP
  		*  fdisk /dev/sdX
 			Type p
 			Type 1
 			Hit enter twice
 			Type t
 			Type 82
 			Type w
 		* mkswap /dev/sdX1
 		* swapon /dev/sdX1
 		* swapon -s (to show)

---
    

##  Extend existing logical volumes 
		-may need to make sure you have extra room in VG (vgdisplay)
		-you can extend up to an  ammount or by an amount
  ## GROW LV
  		* vgextend (if you need to add a PV to a VG)
  		* lvextend -L<size> /dev/vg/lv

---  
    

##  Create and manage Access Control Lists (ACLs) 
		-getfacl to see standard access and ACLs
		-setfact -m to set ACL for a file
		-first need to add acl mount option to file-system where you want to grant ACLs to files
  ## EDIT FSTAB
  		* add ,acl after defaults
  ## REMOUNT FS
   		* mount -o remount <FS>
  ## SET ACL
  		* setfacl -m u:<user>:<access> <file>