Refer to the [ RHCE Network Map ]( rhce_network_map.jpg ) for an overall picture of the network to build.

This document will use the domain name sofreeus.test.

Build router
---
[ answer ]
router has three interfaces: Internet, DMZ, and Corp

machines on the Corp network:
* get their IP configuration from router
* can browse the Internet
* (optional) can reach each other by name by DHCP registration in DNS

do this:
* Set the hostname to 'router.sofreeus.test'.
* Enable routing
* Install, configure, and enable DHCP
* Install, configure, and enable DNS
* Configure the network interfaces in firewall zones as follows:
  * Internet in 'external' zone
  * DMZ in 'dmz' firewall zone
  * Corp in 'home' or 'work' zone

Configure postfix on router to listen on, and relay for, all attached networks.


Build workstation.sofreeus.test
---

workstation boots to a GUI: OS doesn't matter

workstation has one NIC, connected to the 'Corp' network.

Set the hostname to 'workstation.sofreeus.test'.

Should automatically get IP configuration from DHCP.

Test the Intertubes.



Build ipa-master1.sofreeus.test ( or just KDC and DNS, your choice )
---

Build a server with one NIC connected to the 'Corp' network.

Install IPA with DNS to service the domain sofreeus.test

Reconfigure DHCP to give out the new DNS server as the nameserver.

Add users: alice, bob, charlie

Configure station1 as a full IPA client or simple kerberos client. Refresh DHCP and confirm that DNS uses ipa-master1 and works. Confirm that the IPA users can login ( for a simple kerb-client, you'll need to add the user first ).



Build web-app1
---

Build a server with one network interface connected to 'dmz' network.

Install a basic LAMP stack.

Configure 3 name-based virtual hosts (sites with distinct content). At least one site must be a CGI.

Add names to DNS: test.sofreeus.test, sample.sofreeus.test, example.sofreeus.test

Configure SSL/TLS on at least one site. ( You can use self-signed or a cert from IPA. )

Configure privacy ( login ) on at least one site. ( You can use htpasswd or kerberize the site. )

Add local users Dani, Ed, Fegelstein. Create a local group 'test-web-devs'. Configure the 'test' site content directory so that group-members can collaborate effectively. Test thoroughly. New files should be owned and writable by the group.

Block access from a single host (ip.a.ddr.ess/32)

Configure a port-forward on router1 so that all requests for ports 80 and 443 are forwarded to web-app1.

Test that all three sites work



Build san1
---

Build a server with four drives and three NIC's: one connected to 'Corp', two connected to 'Storage'. Configure the NIC's attached to 'Storage' in a team with static IP.

Configure the drives with LVM.

Create a logical volume and provision it as an iSCSI target visible *only* on the Storage network.

Configure postfix on this host to smarthost through router1.


Build nas1
---

Build a server with three NIC's: one connected to 'Corp' and two connected to 'Storage'. Configure the NIC's attached to 'Storage' in a team with static IP.

Configure nas1 to use ipa-master1 as an NTP time-source  ( use chrony ) and as a KDC.

Configure postfix on this host to smarthost through router1.

Initiate a connection to the iSCSI target from san1 on the 'Storage' network.

Create a kerberized NFS export. ( I have no idea. )

Create a kerberized CIFS share. ( I have no idea. Really. )

Configure an NFS export of home directories for alice, bob, and charlie and configure station1 to auto-mount the specific user's home-directory when they log in.
