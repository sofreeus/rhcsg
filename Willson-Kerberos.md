## Configure a system to authenticate using Kerberos (DLW)
---

You want to access this workstation and all machines, using user-accounts that are visible to a domain of machines and services. This is a job for kerberos (and maybe LDAP and other bits).

To practice this, we'll need a kerberos server. I could build *just* a kerberos server and connect to it, but this is a perfect time to get some more practice with FreeIPA / RedHat IdM, which integrates kerberos/LDAP/DNS/PKI beautifully.

Note: RedHat IdM is a derivative of Free IPA. The documents, and I, will refer to them interchangeably. IPA / IdM has an awesome HTTP-based management GUI.

My goals will be to configure an IPA single-master, a full IPA-client workstation, a kerberos-authentication-only workstation (per the exam spec), and a NAS with kerberos authentication for NFS exports and CIFS shares.

### Build the IPA master server

Create a machine on the Corp network. Set RAM to 1GiB or greater.

```bash
# Set hostname
sudo hostnamectl set-hostname DS1.SFS.test

# Configure the network with a static-ip and auto-start
# IP  10.10.10.1/24
# NS  localhost
# GW  10.10.10.254
sudo nmtui
# or
# sudo nmcli ...
# or
# sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
```

Install the packages.

```bash
sudo yum -y install ipa-server ipa-server-dns
```

** This is a good place for a shutdown and snapshot. **

Configure the IPA Server.

```bash
# Maybe add the static-ip fqdn hostname to /etc/hosts
echo 10.10.10.1 DS1.SFS.test DS1 | sudo tee -a /etc/hosts

# Configure
sudo ipa-server-install --setup-dns --forwarder=8.8.8.8 --no-host-dns

# During install, I choose these non-defaults:
# YES, over-write the current bind config.
# YES, continue.
# The configuration took a few minutes to complete.

# Tried it without DNS. Manually copied records to DNS from sample file.
# Need home directories on first login. 'admin' can sign in, but no home-dir
```

Add some users.

```bash
kinit admin
for user in alice bob charlie
do
ipa user-add $user --first=$user --last=willson --password
done
```

Open the firewall.

```bash
sudo yum install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
for service in http https ldap ldaps kerberos dns ntp
do
sudo firewall-cmd --add-service $service
sudo firewall-cmd --add-service $service --permanent
done
sudo firewall-cmd --permanent --add-port 7389/tcp
```

If you run IPA in production, you should configure one or more IPA replicas.

### Configure a machine as an IPA Client

```bash
sudo yum install ipa-client
sudo sed -e 's/ONBOOT=no/ONBOOT=yes/' -i /etc/sysconfig/network-scripts/ifcfg-enp0s3
sudo hostnamectl set-hostname Workstation1.sfs.test
sudo reboot
sudo ipa-client-install --enable-dns-updates
```

Tests:

Can 'admin' log in? Can normal users log in? Can local users log in?

Things to try:

```bash
id alice
getent passwd alice
getent passwd | grep alice
getent group alice
getent group | grep alice
# Note that directory users are findable, but not browse-able.
```

My Results (2014):

* Logins for admin are working, even better after creating a home-dir.
* Logins for alice, bob, and charlie are troubled by expired but unchangeable passwords. With passwords changed at idm-master by admin, they're able to login.
* Logins for dlwillson and root still work fine, no change there.


### Configure a machine as Kerberos client

```bash
sudo yum -y install pam_krb5 krb5-workstation
sudo hostnamectl set-hostname Workstation2.sofreeus.lan
sudo reboot
sudo authconfig-tui
# Note that id someuser does not work
# but kinit someuser does
# as does klist after kinit

# Create local users and kerberos can authenticate them.
sudo useradd alice
sudo useradd admin
# Now, alice and admin can login with their kerberos passwords
# They *don't* need local passwords
```

### Configure a NAS (NFS/CIFS) with Kerberized shares / exports

**On NAS Server**

```bash
sudo yum install ipa-client
sudo hostnamectl set-hostname file-server.sofreeus.lan
sudo reboot
sudo ipa-client-install --enable-dns-updates
sudo reboot
sudo yum groupinstall file-server
```

    # NFS

    # On an IPA admin-station:
    kinit admin
    ipa service-add nfs/file-server.sofreeus.lan@SOFREEUS.LAN

    # On the NFS server:
    sudo kinit admin
    sudo ipa-getkeytab -s idm-master -p nfs/file-server.sofreeus.lan -k /etc/krb5.keytab
    Keytab successfully retrieved and stored in: /etc/krb5.keytab

    # Configure a kerberos-enabled export
    sudo mkdir /home/nfs-export
    sudo chmod --reference=/tmp/ /home/nfs-export
    echo "/home/nfs-export gss/krb5p(rw,no_subtree_check,fsid=0)" | sudo tee -a /etc/exports

    # Open the firewall
    sudo firewall-cmd --add-service nfs --permanent
    $ sudo firewall-cmd --reload
    $ sudo firewall-cmd --list-all

    # Start and enable the service
    $ sudo systemctl restart nfs-server
    $ sudo systemctl enable nfs-server

**On the NFS client**
    $ ...
    # Stuck here. Can't get a mount anywhere. Access denied everywhere.
    # Too many new bits to t-shoot successfully.
    # Taking firewall down helps showmount, but doesn't fix mount.

For NFS, refer to:

 1. https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/Installing_the_IPA_Client_on_Linux.html
 2. 5.3.1. Installing the Client (Full Example)
 3. Step 8. "...set NFS on the client system to work with Kerberos"

    # CIFS
    # FIXME: Finish this section

See also:
---

 * [RHEL7 - Linux Domain Identity, Authentication, and Policy Guide](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Linux_Domain_Identity_Authentication_and_Policy_Guide/index.html)
 * [FreeIPA: Identity/Policy Management](http://docs.fedoraproject.org/en-US/Fedora/18/html/FreeIPA_Guide/index.html)
see also: https://docs.fedoraproject.org/en-US/Fedora/18/html/FreeIPA_Guide/Setting_up_IPA_Replicas.html
