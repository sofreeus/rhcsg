# Build a web application server

Covers the following objectives from the RHCE, EX300:

## HTTP/HTTPS

- Configure a virtual host
- Configure private directories
- Deploy a basic CGI application
- Configure group-managed content
- Configure TLS security

## Install Apache

    $ sudo yum groupinstall "Web Server"
    $ sudo firewall-cmd --add-service=http --permanent
    $ sudo firewall-cmd --add-service=https --permanent
    $ sudo firewall-cmd --reload
    $ sudo systemctl start httpd.service
    $ sudo systemctl enable httpd.service

## Configure a virtual host

Configure a name-based virtual host `public`. When you point your web browser to http://public, it should load the contents of the virtual host and not the default site.

    $ sudo mkdir -p /srv/www/public
    $ sudo su -c 'echo public > /srv/www/public/index.html'

Create a file, `/etc/httpd/conf.d/public.conf` containing:

    <VirtualHost *:80>
            ServerName public

            <Directory /srv/www/public>
                    Require all granted
            </Directory>

            DocumentRoot /srv/www/public

            ErrorLog /var/log/httpd/public_error_log
            TransferLog /var/log/httpd/public_access_log
    </VirtualHost>

Reload the configuration:

    $ apachectl configtest
    $ sudo systemctl reload httpd

In your computer's `/etc/hosts` or `C:\Windows\System32\Etc\Drivers\hosts`, add:

    <ipaddress> public

At this point, using browser go to http://public. It will still load the default page for the Apache installation and not take you to your new virtual host.

If you look at `/var/log/httpd/public_error_log`, you'll see a message similar to:

    [Fri Nov 27 17:24:32.343700 2015] [core:error] [pid 2825] (13)Permission denied: [client 192.168.124.1:45540] AH00035: access to /index.html denied (filesystem path '/srv/www/public/index.html') because search permissions are missing on a component of the path

Fix the SELinux permissions:

    $ sudo chcon -Rv --type=httpd_sys_content_t /srv/www
    $ sudo semanage fcontext -a -t httpd_sys_content_t "/srv/www(/.*)?"

If the semanage command is unavailable:

    $ sudo yum install policycoreutils-python

Browsing to http://public will now load our virtual host page.

## Configure private directories

Let's create a name-based virtual host `private`. We will secure this site behind a username and password prompt.

    $ sudo mkdir /srv/www/private
    $ sudo su -c 'echo private > /srv/www/private/index.html'

Create a password list, and grant users `alice` and `bob` access:

    $ sudo mkdir /etc/httpd/auth
    $ sudo htpasswd -c /etc/httpd/auth/private.htpasswd alice
    $ sudo htpasswd /etc/httpd/auth/private.htpasswd bob

Create `/etc/httpd/conf.d/private.conf`:

    <VirtualHost *:80>
            ServerName private

            <Directory /srv/www/private>
                    AuthType Basic
                    AuthName "Secret squirrel access only"
                    AuthUserFile /etc/httpd/auth/private.htpasswd
                    Require valid-user
            </Directory>

            DocumentRoot /srv/www/private

            ErrorLog /var/log/httpd/private_error_log
            TransferLog /var/log/httpd/private_access_log
    </VirtualHost>

Reload the Apache config:

    $ apachectl configtest
    $ sudo systemctl reload httpd

In your computer's `/etc/hosts` or `C:\Windows\System32\Etc\Drivers\hosts`, add `private` to the same line as `public`:

        <ipaddress> public private

Point your computer's web browser to http://private and log in with either alice or bob's credentials.

## Deploy a basic CGI application

Create the directory and set the SELinux context:

    $ sudo mkdir /srv/www/public/cgi-bin
    $ sudo chcon -R --type=httpd_sys_script_exec_t /srv/www/public/cgi-bin
    $ sudo semanage fcontext -a -t httpd_sys_script_exec_t "/srv/www/public/cgi-bin(/.*)?"

Create the file `/srv/www/public/cgi-bin/hello.cgi`:

    #!/usr/bin/python
    print "Content-type:text/html\r\n\r\n"
    print "Hello world!"

Add exec permissions:

    $ sudo chmod +x /srv/www/public/cgi-bin/hello.cgi

Add the following in the `VirtualHost` section of `/etc/httpd/conf.d/public.conf`:

    <Directory /srv/www/public/cgi-bin>
            Options +ExecCGI
            AddHandler cgi-script .cgi
    </Directory>

Find the ScriptAlias directive in `/etc/httpd/conf/httpd.conf` and commentit out:

    #ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

Reload the config:

    $ apachectl configtest
    $ sudo systemctl reload httpd

Point your web browser to http://public/cgi-bin/hello.cgi

## Configure group-managed content

Carol and Dan are in the accounting department, and want their own folder on the public site to place web stuff.

    $ sudo groupadd accounting
    $ sudo useradd -G accounting carol
    $ sudo useradd -G accounting dan
    $ sudo mkdir /srv/www/public/accounting
    $ sudo chown nobody:accounting /srv/www/public/accounting/
    $ sudo chmod g+ws /srv/www/public/accounting/
    $ ls -ld /srv/www/public/accounting/
    drwxrwsr-x. 2 nobody accounting 6 Nov 28 07:16 /srv/www/public/accounting/
    $ sudo su - carol -c 'echo accounting > /srv/www/public/accounting/index.html'
    $ ls -l /srv/www/public/accounting/
    total 4
    -rw-rw-r--. 1 carol accounting 11 Nov 28 07:24 index.html

Browse to http://public/accounting/

## Configure TLS security

Create a new virtual host named `secure` and secure it with a TLS certificate. Also, all non-https requests to the secure virtual host should be redirected to https.

Create the files for the virtual host:

    $ sudo mkdir /srv/www/secure
    $ sudo su -c 'echo secure > /srv/www/secure/index.html'

Generate a certificate, and sign it ourselves:

    $ sudo mkdir /etc/httpd/tls
    $ sudo openssl genrsa -out /etc/httpd/tls/secure.key 2048
    $ sudo chmod 0600 /etc/httpd/tls/secure.key
    $ sudo openssl req -new -sha256 -key /etc/httpd/tls/secure.key -out /etc/httpd/tls/secure.csr
        # Complete the fields as however you like, except common name should be secure, and don't set a challenge password
    $ sudo openssl x509 -req -days 365 -in /etc/httpd/tls/secure.csr -signkey /etc/httpd/tls/secure.key -out /etc/httpd/tls/secure.crt

Create a file, `/etc/httpd/conf.d/secure.conf`:

    <VirtualHost *:80>
            ServerName secure
            Redirect permanent / https://secure/
    </VirtualHost>

    <VirtualHost *:443>
            ServerName secure

            SSLEngine on

            <Files ~ "\.(cgi|shtml|phtml|php?)$">
                SSLOptions +StdEnvVars
            </Files>

            BrowserMatch "MSIE [2-5]" \
                     nokeepalive ssl-unclean-shutdown \
                     downgrade-1.0 force-response-1.0

            SSLCertificateKeyFile /etc/httpd/tls/secure.key
            SSLCertificateFile /etc/httpd/tls/secure.crt

            <Directory /srv/www/secure>
                    Require all granted
            </Directory>

            <Directory /srv/www/secure/cgi-bin>
                    Options +ExecCGI
                    AddHandler cgi-script .cgi
            </Directory>

            DocumentRoot /srv/www/secure

            ErrorLog /var/log/httpd/secure_error_log
            TransferLog /var/log/httpd/secure_access_log
    </VirtualHost>

Reload Apache:

    $ apachectl configtest
    $ sudo systemctl reload httpd

In your computer's `/etc/hosts` or `C:\Windows\System32\Etc\Drivers\hosts`, add `private` to the same line as `public`:

        <ipaddress> public private secure

Point your browser to http://secure. You will be redirected to https://secure. You will get a certificate warning (because it is self-signed). Examine the certificate and determine that it is the one you created.

Yay!

# BONUS!

## PHP

Let's add PHP support to our web server!

    $ sudo su -c 'echo "<?php phpinfo(); ?>" > /srv/www/public/info.php'
    $ sudo yum install php
    $ sudo systemctl restart httpd

Point your browser to: http://public/info.php

Wasn't that easy?
