**SMTP/Postfix**

Configure a system to forward all email to a central mail server
----------------------------------------------------------------

-Postfix should be installed and running by default, with necessary SELinux
 and firewall ports open.

-To set up a RHEL sever to forward mail, inject settings into the postfix 
 config *main.cf* file with the *postconf -e* command.

-If you knew what the relevant configurable parameters might be, you can list
 with the *postconf* command followed by the parameters space seperated.

-If you don't know what the parameter name might be, you can list all of them
 with the *postconf* command.

-If you want a description of what they do, or valid settings,
 less /etc/postfix/main.cf

-After changing a postfix parameter, reload or restart postfix to have them 
 take effect ('postfix reload' or perhaps better, *systemctl restart postfix*)

-Use *postqueue -p* to list anything in the outgoing mailqueue for the logged
 in user

-Use *postqueue -f* to try to resend (instead of waiting for the next retry interval)


------------
  eg.

[root@science rhcsg]# systemctl status postfix
...
  master (pid  2495) is running...
...  
[root@science rhcsg]# postconf relayhost inet_interfaces myorigin mynetworks
relayhost = 
inet_interfaces = localhost
myorigin = $myhostname
mynetworks = 127.0.0.0/8 [::1]/128 
[root@science rhcsg]# 
[root@science rhcsg]# postconf myhostname
myhostname = science.localdomain
[root@science rhcsg]# postqueue  -p
Mail queue is empty
[root@science rhcsg]# 
[root@science rhcsg]# mail -s "test" dlwillson@sofreeus.org < /etc/redhat-release 

[root@science rhcsg]# postqueue  -p
-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------
C12BB84084F      476 Sun Feb  8 10:03:18  root@science.localdomain
(Host or domain name not found. Name service error for name=sofreeus.org type=MX: Host not found, try again)
                                         dlwillson@sofreeus.org

-- 0 Kbytes in 1 Request.
[root@science rhcsg]# 


------------
Config system to forward mail to a corp. mail server, with other modifications.

The corp mail server is dlw.mailsrv.com

To specify mail be relayed to this server, and not base it on a MX lookup from DNS...


   get before settings (might save /etc/postfix/master.cf and /etc/postfix/main.cf)
    # postconf

   point to relay to corp mail server (brackets mean it won't check DNS MX record)
    # postconf -e "relayhost=[dlw.mailsrv.com]"

   ensure that servers on the same subnet as yours, don't use your server to forward
   mail. i.e., only this host's mail.
     # postconf -e "inet_interfaces=loopback-only"

   make it appear that messages comimg from this host are originating from another
   host.
     # hostname
     rich.domain.com
     # postconf -e "myorigin=mshoupe.domain.com"

   mail server and mail account name should be provided in test, so you can test 
   outbound mail using mail client like *mail* or *mailx*. Consult man pages.


