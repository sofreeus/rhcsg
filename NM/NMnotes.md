## __NetworkManager service and network.service notes__
    -------------------------------------------------


##  Questions I tried to answer via testing
   /these are general 'real life' questions, and may not be relevant to the exam./ 

		- Do the legacy 'network' init service and the newer NetworkManager systemd service both get installed, loaded and started by default?
		**ON MY SYSTEM THEY WE'RE BOTH PRESENT, BUT ONLY NM WAS IN A RUNNING STATE**

		- How do I check for these and/or how do I know if an interface is under network.service control or NetworkManager control?
		**systemcli and 'nmcli dev status'**

		- Does NetworkManager via it's configuration tools (eg. nmtui) make changes to the traditional network-scripts interface files?
		**YES, BUT DON'T KNOW IF ALL CHANGES MADE THROUGH NMTUI ARE REFLECTED IN FILES**
		**DON'T KNOW IF NM SAVES CHANGES INSIDE NM**

		- If NM does edit and therefore use these files, is that all that NM relies on? Does it have it's own repository of network data, or is it all in the traditioal /etc/sysconfig files?
		**UNDETERMAINED**

		- If a network-script interface configuration file does not contain a line for NM_CONTROLLED, is the default vaule "yes" unless otherwise specified? (this would tell an interface whether is controlled by network.service or NM)
		**IT APPEARS TO BE 'yes' UNLESS EXPLICITLY SPECIFIED OTHERWISE** 

		- If configuring an interface to be a static IP (DHCP disabled), is it preferred to manually edit network-script files and use the legacy network.service?
		**DOCUMENTATION AND USER EXPERIENCE SEEMS TO SUGGEST THIS**

		- On a server with no wireless interface, is using legacy network.service prefered over NM?
		**DOCUMENTATION AND USER EXPERIENCE SEEMS TO SUGGEST THIS**

		- If using NM via it's config tool nmtui to configure a static IP on an inteface, do I still need to manually edit the corresponding network-script config file? (delete unecessary lines, add lines, alter values)
		**UNSURE. nmtui DOESN'T SEEM TO SET BOOTPROTO=static. I EDITED THAT LINE POST nmtui**

		- In a sysconfig/network-script interface config file, is BOOTPROTO=none and BOOTPROTO=static synonymous?
		**UNSURE**

		- If you are only configuring static IPs on interfaces and using netwrok.service, should NM be disabled?
		**DOCUMENTATION AND USER EXPERIENCE SEEMS TO SUGGEST THIS**

		- After making network entires/edits via NM tools, which changes are run-time, and which need NetworkManager service restarted to take effect?
		**IF NM ISN'T RUNNING AND YOU ARE MAKING ALL CHANGES BY EDITING FILES, THEN network
		  SERVICE NEEDS TO BE RESTARED. CHANGES I MADE VIA nmtui DIDN'T SEEM TO NEED A 
		  RESTART OF NetworkManger, BUT I WAS ONLY DEALING WITH ADDING A STATIC IP**



## Experiments I ran
   /The screenshot images give examples/

		-  looked at all the network settings before any changes were made

				-  'ip link' to see what interfaces are present
				-  'ip add' to see if there were any IP4 and/or IPv6 addresses on any interface
				-  'cat /etc/sysconfig/network-scripts/ifcfg-<nic>' to get out-of-the-box 
				    file settings, and compare if any changes happen when using NM tools
				-  'systemctl list-units --type=service | grep -i network' to see the 
				    state of network services
				-  'nmcli dev status' to see which interfaces are controlled by NM.
				    if they are in th output, they under NM control

		-  used NM config tool 'nmtui' to put static IP on interface and other settings 
		

		-  looked at all the network settings after changes were made (see above)

## Observations & notes

		- Both network and NetworkManager seems to be installed and loaded by default, however network is in an exited stated and NM is running. Maybe the systems needs network to up certain things before NM takes control?

		- nmtui seemed to be installed by default, but in case it isn't, ...
			'yum install NetworkManager-tui'

		- nmtui for setting a static IP on an interface notes:
				-there is no field for netmask; you use CIRD notation on the IP address
				-there's a drop-down field of 5 different network types. Of these, only
				 'automatic connect' and 'manual' are vialid for a IP4 static ethernet.
				-'automatic connet' means DHCP and alters the corresponding
				  network-script file's BOOTPROTO vaule to 'dhcp'
				-'manual' is the same as static *I think*. Alters the
				  network-script file's BOOTPROTO value to 'none'

		- There is plenty out there in the vein of 'don't use NM for static routes' and 'don't use NM on enterprise servers' and 'NM is only for DHCP' and 'NM is for PCs and laptops and for WiFi' etc.

## To disable NM

		Disable NetworkManager with:

		systemctl disable NetwokManager
		systemctl stop NetworkManager 
		Edit /etc/sysconfig/network-scripts/ifcfg-<nic> and make sure it says
			NM_CONTROLLED=no
			ONBOOT=yes

		so that the network service handles it and that the interface starts on boot, run
		systemctl enable network.service
		systemctl start network.service

