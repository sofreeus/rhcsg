**SELinux man pages**
--------------------------------------------

If you want to load all the SELinux man pages to have help with relevant booleans and file contexts.

    # man -k _selinux | wc -l
    1

    # yum install selinux-policy-devel -y

    # man -k | wc -l
    804

  eg

    Install selinux man pages

    # man -k nfsd_selinux
        (not there)

    # yum install selinux-policy-devel

    # mandb

    # man -k nfsd_selinux
        (there now)
