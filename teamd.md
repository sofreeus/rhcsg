**TEAMD from command line**

This is the shortcut I'm going to memorize for the test
-------------------------------------------------------


1) _Grab pertitent information from man pages_


  a. Get runner JSON syntax

    # man teamd.conf | grep '"runner"'
        "runner": {"name": "roundrobin"}
        "runner": {"name": "activebackup"}
        "runner": {"name": "loadbalance"}

   Remember that these examples need to be enclosed in curly braces and single quoted, eg.
        "runner": {"name": "activebackup"}
           becomes
        '{"runner": {"name": "activebackup"}}'
           in the JSON config

   If you want to know all the available runners, they are listed in the same teamd.conf man page
   (search for string "The following runners are available")
 


  b. Get nmcli syntax for creating team and also for adding slaves to team (step c.)

    # man nmcli-examples | grep team
        $ nmcli con add type team con-name Team1 ifname Team1 config team1.master.json.conf
        $ nmcli con add type team-slave con-name Team1-slave1 ifname em1 master Team1
        $ nmcli con add type team-slave con-name Team1-slave2 ifname em1 master Team1

   The first example line is for creating a team. 
   In this example, they are pointing to a file with the runner JSON information. In your case, you will just put it on the command line. 
   In this example, they call both the "soft" name and interface name, the same thing; Team1. The test may have you name it something else.

   Combining the output of the two man pages, your command to create a team may look like..

        # nmcli con add type team con-name TEAM0 ifname TEAM0 config '{"runner": {"name":/ "roundrobin"}}'' 



  c. Use the output in b. to create slave devices to add to the team you created



2) _Use the information from man pages to create teamed interfaces_

   Using output from 1a to get JSON runner syntax, and 1b to get both team a nd slave creation,
   create a teamd device like the following example...

       # ip link
           (shows no Team0)
       
       # nmcli con show
           (shows no Team0)

       # nmcli con add type team con-name Team0 ifname Team0 config '{"runner": {"name":/ "roundrobin"}}''
           (tells you is was created successfully, and outputs UUID of Team0)

       # ip link
           (now shows Team0)

       # nmcli con show
           (now shows Team0)   note: if ifname were different than con-name, ip and 
                                     ifconfig would show this difference.

       # teamdctl Team0 state
           (will only output what runner is accosiated, at this state)

       # nmcli con show Team0
           (will also tell you what runner, as well as the UUID and whether it's static or DHCP, and other information. IPv4.method will be auto, meaning DHCP)

   If yo want the team to be static instead if it's default 'auto', you can modify that now. Consult man page for syntax if need be. Or, you can edit the network-script ifcfg file later.
   Commands must be done in tis order.

       # nmcli con mod Team0 ipv4.address 192.168.122.11/24
       # nmcli con mod Team0 ipv4.method manual                 (manual = static)

   Give the real interface an alias (port name), and assign to team (from 1b output)

       # nmcli con add type team-slave con-name Team0-port0 ifname eth0 master Team0
       # nmcli con add type team-slave con-name Team0-port1 ifname eth1 master Team0
           (will say succefully added and give a UUID for the port)

       # ip link
           (Team0 link will now say UP)

       # nmcli con show Team0
           (will now show the statis IP address accociated with Team0)

       # teamdctl Team0 state
           (will now show the two port interfaces accociated with Team0, and if its UP)


3) _Bring up/down Team, ports & interfaces_

  To bring up and down and interface e.g., 
        nmcli con <up/down> Team0

  To bring up 'connect' interface e.g.
        nmcli dev con eth1

  To bring down 'disconnect' interface e.g.
        nmcli dev dis eth1

  Staring the port interfaces automatically starts thier associated team interface

