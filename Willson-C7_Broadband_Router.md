## Use firewalld and associated mechanisms such as rich rules, zones and custom rules, to implement packet filtering and configure network address translation (NAT) (DLW)
---

You might want to use RHEL7 as a broadband router.
The next sections show how to setup DHCP, DNS, NAT, routing, and port forwards.

Network Interfaces:

    Router1      : C7 minimal : bridged (DHCP), internal-DMZ (172.16.254.254), internal-Corp (10.10.10.254)
    WebApp1      : C7 minimal : internal-DMZ (172.16.254.101)
    Workstation1 : C7 GNOME   : internal-Corp (DHCP)

Networks:

    Internet = unspecified     ( Router1::Internet is configured by DHCP )
    DMZ      = 172.16.254.0/24 ( Router1::DMZ does *not* provide DHCP )
    Corp     = 10.10.10.0/24   ( Router1::Corp provides DHCP )

[Network](rhce_network_map.jpg)

Router1 has highest address (...254) on DMZ and Corp.

WebApp1 has .101 on DMZ

Workstation1 is a DHCP client

You'll provide routing as follows:

* All traffic relating to ongoing sessions will be allowed.
* All traffic from 'Corp' for 'DMZ' will be allowed.
* All traffic from 'Corp' for any other network will be NAT'd.
* HTTP/HTTPS traffic for a web-server in 'DMZ' will be allowed.

You'll need at least three machines to set all this up and test it.

* 'Workstation1' - a workstation on the trusted network
* 'WebApp1'      - a web-server on the DMZ network
* 'Router1'      - a router with a route to Internet with interfaces on Corp and DMZ

##  Configure Router1

    # Set hostname to 'Router1 ' and configure static ip addresses as needed
    Router1 $ sudo systemctl stop network
    Router1 $ sudo nmtui
    # Corp 10.10.10.254/24
    # DMZ 172.16.254.254/24
    # No Gateways or DNS needed on these interfaces.
    Router1 $ sudo systemctl start network
    Router1 $ ip a
    # Confirm

    # Add DHCP daemon
    # It'll be easier to give this box as the default route on others
    Router1 $ sudo yum install dhcp
    # Add a basic configuration
    Router1 $ cat /usr/share/doc/dhcp-4.2.5/dhcpd.conf.example | sudo tee -a /etc/dhcp/dhcpd.conf
    Router1 $ sudo vim /etc/dhcp/dhcpd.conf
    Router1 $ sudo cat /etc/dhcp/dhcpd.conf
        authoritative;

        # Corp
        subnet 10.10.10.0 netmask 255.255.255.0 {
          range 10.10.10.101 10.73.73.199;
          option routers 10.73.73.254;
          option domain-name "sfs.test";
          option domain-name-servers 8.8.8.8;
        }

    Router1 $ sudo systemctl restart dhcpd
    Router1 $ sudo systemctl enable dhcpd

    # Configure Router1 to route between networks
    # check routing status
    Router1 $ cat /proc/sys/net/ipv4/ip_forward
    # enable routing in running kernel
    Router1 $ echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward
    # read the setting out into a system settings file to make it permanent
    Router1 $ sysctl -a | grep ip_forward | sudo tee /etc/sysctl.d/90-enable-routing.conf
    Router1 $ cat /etc/sysctl.d/90-enable-routing.conf
        net.ipv4.ip_forward = 1

    # Configure firewall / NAT
    Router1 $ sudo yum install firewalld
    Router1 $ cd /etc/sysconfig/network-scripts/
    Router1 $ echo ZONE=external | sudo tee -a ifcfg-public
        ZONE=external
    Router1 $ echo ZONE=home | sudo tee -a ifcfg-corp
        ZONE=home
    Router1 $ echo ZONE=dmz | sudo tee -a ifcfg-dmz
        ZONE=dmz

    # Configure firewall ( firewalld / firewall-cmd )
    Router1 $ sudo reboot
    ...
    Router1 $ sudo firewall-cmd --get-active-zones

NAT should turn on automatically.

Connect Workstation1 to Corp and test. You should have Internet.

Connect WebApp1 to DMZ and test. You should have Internet.

WebApp1 and Workstation1 should be able to ping one another by address through Router1 .

### Forwarding ports 80 and 443 to WebApp1

#### Build a functioning test application on WebApp1.

    WebApp1 $ sudo yum groupinstall webserver
    WebApp1 $ sudo yum install firewalld
    WebApp1 $ sudo firewall-cmd --add-service http
    WebApp1 $ sudo firewall-cmd --add-service https
    WebApp1 $ sudo firewall-cmd --add-service http --permanent
    WebApp1 $ sudo firewall-cmd --add-service https --permanent
    WebApp1 $ sudo systemctl start httpd
    WebApp1 $ sudo systemctl status httpd
    WebApp1 $ sudo systemctl enable httpd
    WebApp1 $ curl localhost

Optionally, build a nice website.

#### Configure Router1 to forward ports 80 and 443 to WebApp1's IP address.

    Router1 $ sudo firewall-cmd \
              --zone=external \
              --add-forward-port port=80:proto=tcp:toaddr=172.16.254.101
    Router1 $ sudo firewall-cmd \
              --permanent \
              --zone=external \
              --add-forward-port port=80:proto=tcp:toaddr=172.16.254.101
    Router1 $ sudo firewall-cmd \
              --zone=external \
              --add-forward-port port=443:proto=tcp:toaddr=172.16.254.101
    Router1 $ sudo firewall-cmd \
              --permanent \
              --zone=external \
              --add-forward-port port=443:proto=tcp:toaddr=172.16.254.101


Now, from any machine that can reach Router1's "Internet" address, browse to (or curl) the Internet interface address of Router1. The request should be forwarded to WebApp1 and you should see the web-page.


Final Tests
---

Reboot everything. Confirm that it all continues to work after a reboot.

See also:

 * https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/sec-Using_Firewalls.html
 * http://www.certdepot.net/rhel7-get-started-firewalld/

